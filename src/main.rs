use format_equivalent::compare::{ordered_equal, Comparison};
use format_equivalent::runner::{create_child_process, map_stdout_child, ScriptError};
use regex::Regex;
use std::process::{ExitStatus, Stdio};
use std::time::Duration;
use tokio::fs::File as TokioFile;
use tokio::io::{AsyncBufReadExt, BufReader};

#[tokio::main]
async fn main() {
    let result_child = create_child_process(
        "cat 'data/received.csv'",
        Stdio::null(),
        Stdio::piped(),
        Stdio::null(),
    );

    let cur_child = match result_child {
        Ok(child) => child,
        Err(script_error) => {
            println!("{}", script_error);
            return;
        }
    };

    let result = map_stdout_child(cur_child, Duration::from_secs(60), move |stdout| {
        async {
            let regex_csv = Regex::new(
                "^\
                 ([^,]*),[[:space:]]*\
                 ([^,]*),[[:space:]]*\
                 ([^,]*),[[:space:]]*\
                 ([^,]*),[[:space:]]*\
                 ([^,]*),[[:space:]]*\
                 ([^,]*)$",
            )
            .unwrap();

            let expected_file = TokioFile::open("data/expected.csv")
                .await
                .expect("couldn't open compare file");
            let received_lines = BufReader::new(stdout).lines();
            let expected_lines = BufReader::new(expected_file).lines();

            ordered_equal(expected_lines, received_lines, regex_csv).await
        }
    });

    let output: Result<(Comparison, ExitStatus), ScriptError> = result.await;

    match output {
        Ok((comparison, status)) => {
            println!("ExitStatus: {:?}", status.code());
            println!("{}", comparison)
        }
        Err(script_error) => println!("Error: {:?}", script_error),
    }
}
