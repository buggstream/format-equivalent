use std::future::Future;
use std::process::{self, ExitStatus, Stdio};
use std::string::FromUtf8Error;
use std::time::Duration;
use thiserror::Error;
use tokio::process::{Child, ChildStdout, Command};
use tokio::time;

#[derive(Error, Debug)]
pub enum ScriptError {
    #[error("Error while executing script: '{0}'")]
    IOError(#[from] std::io::Error),
    #[error("{0}")]
    Timeout(#[from] tokio::time::Elapsed),
    #[error("{0}")]
    FromUtf8Error(#[from] FromUtf8Error),
    #[error("The standard output was not be captured.")]
    OutputNotCaptured,
}

pub fn pipe_children(script1: &str, script2: &str) -> Result<Child, ScriptError> {
    let child1 = create_child_process_std(script1, Stdio::null(), Stdio::piped(), Stdio::null())?;
    let stdout = child1.stdout.ok_or(ScriptError::OutputNotCaptured)?;
    create_child_process(script2, Stdio::from(stdout), Stdio::piped(), Stdio::null())
}

pub async fn read_stdout_child(
    child: Child,
    timeout: Duration,
) -> Result<(String, ExitStatus), ScriptError> {
    let output = time::timeout(timeout, child.wait_with_output())
        .await
        .map_err(ScriptError::Timeout)?
        .map_err(ScriptError::IOError)?;
    Ok((
        String::from_utf8(output.stdout).map_err(ScriptError::FromUtf8Error)?,
        output.status,
    ))
}

pub async fn map_stdout_child<ReturnValue, FutureResult>(
    mut child: Child,
    timeout: Duration,
    func: impl Fn(ChildStdout) -> FutureResult,
) -> Result<(ReturnValue, ExitStatus), ScriptError>
where
    FutureResult: Future<Output = Result<ReturnValue, std::io::Error>>,
{
    time::timeout(timeout, async {
        if let Some(stdout) = child.stdout.take() {
            let result: Result<ReturnValue, std::io::Error> = func(stdout).await;
            let return_value = result.map_err(ScriptError::IOError)?;
            let exit_status = child.await.map_err(ScriptError::IOError)?;
            Ok((return_value, exit_status))
        } else {
            Err(ScriptError::OutputNotCaptured)
        }
    })
    .await
    .map_err(ScriptError::Timeout)?
}

pub fn create_child_process(
    script: &str,
    stdin: Stdio,
    stdout: Stdio,
    stderr: Stdio,
) -> Result<Child, ScriptError> {
    bash_command()
        .arg(script)
        .stdin(stdin)
        .stdout(stdout)
        .stderr(stderr)
        .spawn()
        .map_err(ScriptError::IOError)
}

pub fn create_child_process_std(
    script: &str,
    stdin: Stdio,
    stdout: Stdio,
    stderr: Stdio,
) -> Result<process::Child, ScriptError> {
    bash_command_std()
        .arg(script)
        .stdin(stdin)
        .stdout(stdout)
        .stderr(stderr)
        .spawn()
        .map_err(ScriptError::IOError)
}

#[inline]
fn bash_command() -> Command {
    let mut command = Command::new("bash");
    command.arg("-c");
    command
}

#[inline]
fn bash_command_std() -> process::Command {
    let mut command = process::Command::new("bash");
    command.arg("-c");
    command
}
