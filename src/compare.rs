use regex::{Captures, Regex};
use std::fmt::{Debug, Display, Error, Formatter};
use tokio::stream::{Stream, StreamExt};

#[derive(Eq, PartialEq, Debug)]
pub enum Comparison {
    Equal,
    NotEqual { message: String },
}

impl Comparison {
    fn new_not_equal<Expected: Debug, Received: Debug>(
        expected: &Expected,
        received: &Received,
    ) -> Comparison {
        Comparison::NotEqual {
            message: format!("Expected:\n{:?}\nbut received:\n{:?}", expected, received),
        }
    }
}

impl Display for Comparison {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match self {
            Comparison::Equal => write!(f, "Equal"),
            Comparison::NotEqual { message } => write!(f, "NotEqual {{\n{}\n}}", message),
        }
    }
}

pub trait Comparator<Expected: ?Sized, Received: ?Sized> {
    fn compare(&self, expected: &Expected, received: &Received) -> Comparison;
}

impl Comparator<String, String> for Regex {
    #[inline]
    fn compare(&self, expected: &String, received: &String) -> Comparison {
        let capture_expected = self.captures(expected);
        let capture_received = self.captures(received);

        capture_comparison(capture_expected, capture_received)
    }
}

pub async fn ordered_equal<StreamExpected, StreamReceived, StreamError, Expected, Received>(
    mut expected_stream: StreamExpected,
    mut received_stream: StreamReceived,
    comparator: impl Comparator<Expected, Received>,
) -> Result<Comparison, StreamError>
where
    StreamExpected: Unpin + Stream<Item = Result<Expected, StreamError>>,
    // StreamExpected: Unpin + TryStream<Ok = Expected, Error = StreamError>,
    StreamReceived: Unpin + Stream<Item = Result<Received, StreamError>>,
{
    let mut current_item = (
        expected_stream.try_next().await?,
        received_stream.try_next().await?,
    );
    while let (Some(expected), Some(received)) = current_item {
        let capture_comparison = comparator.compare(&expected, &received);

        if capture_comparison != Comparison::Equal {
            return Ok(capture_comparison);
        }

        current_item = (
            expected_stream.try_next().await?,
            received_stream.try_next().await?,
        );
    }

    match current_item {
        (None, Some(_)) => Ok(Comparison::NotEqual {
            message: String::from("Received stream is longer than expected."),
        }),
        (Some(_), None) => Ok(Comparison::NotEqual {
            message: String::from("Received stream is shorter than expected."),
        }),
        _ => Ok(Comparison::Equal),
    }
}

fn capture_comparison(
    expected_capture_option: Option<Captures>,
    received_capture_option: Option<Captures>,
) -> Comparison {
    match (expected_capture_option, received_capture_option) {
        (None, None) => Comparison::Equal,
        (None, Some(received_capture)) => {
            let message = capture_groups_iter(&received_capture)
                .map(|group| format!("{:?}", group))
                .collect::<Vec<String>>()
                .join(", ");

            Comparison::NotEqual {
                message: format!("Expected no capture, but received capture\n{}", message),
            }
        }
        (Some(expected_capture), None) => {
            let message = capture_groups_iter(&expected_capture)
                .map(|group| format!("{:?}", group))
                .collect::<Vec<String>>()
                .join(", ");

            Comparison::NotEqual {
                message: format!("Received no capture, but expected capture\n{}", message),
            }
        }
        (Some(expected_capture), Some(received_capture)) => {
            let mut expected_iter = capture_groups_iter(&expected_capture);
            let mut received_iter = capture_groups_iter(&received_capture);

            loop {
                match (expected_iter.next(), received_iter.next()) {
                    (None, None) => return Comparison::Equal,
                    (Some(expected_group), Some(received_group)) => {
                        if expected_group == received_group {
                            continue;
                        }

                        return Comparison::new_not_equal(&expected_group, &received_group);
                    }
                    (Some(expected_group), None) => {
                        return Comparison::NotEqual {
                            message: format!(
                                "Received no group, but expected group\n{:?}",
                                expected_group
                            ),
                        };
                    }
                    (None, Some(received_group)) => {
                        return Comparison::NotEqual {
                            message: format!(
                                "Expected no group, but received group\n{:?}",
                                received_group
                            ),
                        };
                    }
                }
            }
        }
    }
}

fn capture_groups_iter<'a>(capture: &'a Captures<'a>) -> impl Iterator<Item = Option<&'a str>> {
    capture
        .iter()
        .skip(1) // Skip the first one, since that is the whole matched string (not the individual groups)
        .map(|option_match| option_match.map(|matched| matched.as_str()))
}
